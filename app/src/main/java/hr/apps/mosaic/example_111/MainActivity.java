package hr.apps.mosaic.example_111;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

	private static final int REQUEST_LOCATION_PERMISSION = 10;
	private static final int MIN_TIME = 1000;
	private static final int MIN_DISTANCE = 10;

	@BindView(R.id.bShowLocation) Button bShowLocation;
	@BindView(R.id.tvLocationDisplay) TextView tvLocationDisplay;

	LocationManager mLocationManager;
	LocationListener mLocationListener;
	String mLocationProvider;
	boolean mTrackingLocation = false;
	private Criteria mCriteria;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		this.initialize();
	}

	@OnClick(R.id.bShowLocation)
	public void showLocationClicked(Button bShowLocation) {
		if(this.mTrackingLocation == false) {
			if (this.hasLocationPermission()) {
				startTrackingLocation();
			} else {
				this.requestPermission();
			}
		}
		else {
			this.stopTrackingLocation();
		}
	}

	private void initialize() {
		this.mCriteria = new Criteria();
		this.mCriteria.setAccuracy(Criteria.ACCURACY_FINE);
		this.mLocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
		this.mLocationListener = new SimpleLocationListener();
	}

	//region LocationTracking

	private void startTrackingLocation(){
		this.mLocationProvider = this.mLocationManager.getBestProvider(this.mCriteria,true);
		this.mLocationManager.requestLocationUpdates(
				this.mLocationProvider, MIN_TIME, MIN_DISTANCE, this.mLocationListener);
		this.mTrackingLocation = true;
		bShowLocation.setText(getString(R.string.bStopTrackingLocationText));
	}

	private void stopTrackingLocation() {
		this.mLocationManager.removeUpdates(this.mLocationListener);
		this.mTrackingLocation = false;
		bShowLocation.setText(getString(R.string.bStartTrackingLocationText));
	}

	private void updateLocationDisplay(Location location) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Lat: ").append(location.getLatitude()).append("\n");
		stringBuilder.append("Lon: ").append(location.getLongitude());
		this.tvLocationDisplay.setText(stringBuilder.toString());
	}

	private class SimpleLocationListener implements LocationListener{

		@Override
		public void onLocationChanged(Location location) {
			updateLocationDisplay(location);
		}

		@Override public void onStatusChanged(String provider, int status, Bundle extras) {}

		@Override public void onProviderEnabled(String provider) { }

		@Override public void onProviderDisabled(String provider) {}
	}

	//endregion

	//region PermissionManagement
	private boolean hasLocationPermission(){
		String LocationPermission =
				Manifest.permission.ACCESS_FINE_LOCATION;
		int status =
				ContextCompat.checkSelfPermission(this,LocationPermission);
		if(status == PackageManager.PERMISSION_GRANTED){
			return true;
		}
		return false;
	}

	private void requestPermission() {

		String[] permissions = new String[]{
				Manifest.permission.ACCESS_FINE_LOCATION
		};

		ActivityCompat.requestPermissions(MainActivity.this,
				permissions, REQUEST_LOCATION_PERMISSION);
	}

	@Override
	public void onRequestPermissionsResult(
			int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode){
			case REQUEST_LOCATION_PERMISSION:
				if(grantResults.length >0){
					if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
						Log.d("Permission","Permission granted. User pressed allow.");
						startTrackingLocation();
					}
					else{
						Log.d("Permission","Permission not granted. User pressed deny.");
						askForPermission();
					}
				}
				break;
		}
	}

	private void askForPermission(){
		boolean shouldExplain =
				ActivityCompat.shouldShowRequestPermissionRationale(
				MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
		if(shouldExplain){
			Log.d("Permission","Permission should be explained, - don't show again not clicked.");
			this.displayDialog();
		}
		else{
			Log.d("Permission","Permission not granted. User pressed deny and don't show again.");
			tvLocationDisplay.setText("Sorry, we really need that permission");
		}
	}

	private void displayDialog() {
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		dialogBuilder.setTitle("Location permission")
				.setMessage("We display your location and need your permission")
				.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Log.d("Permission", "User declined and won't be asked again.");
						dialog.cancel();
					}
				})
				.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Log.d("Permission","Permission requested because of the explanati-on.");
						requestPermission();
						dialog.cancel();
					}
				})
				.show();
	}

	//endregion
}
